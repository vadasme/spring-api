# Api rest construida en spring boot

Este sistema back-end utiliza las siguientes dependencias:
-Spring Web
-Lombok
-JPA
-Spring Dev Tools
-Mysql

Para ejecutar el sistema solo es necesario crear una base de datos con el nombre 'bluelatam_fase2' y referenciar el servidor de aquella en application.properties.
