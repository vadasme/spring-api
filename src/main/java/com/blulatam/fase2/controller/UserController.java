package com.blulatam.fase2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.blulatam.fase2.entity.User;
import com.blulatam.fase2.service.UserService;

@RestController 
@RequestMapping (value = "/user")

@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class UserController {
	@Autowired
    private UserService userService;
	
	@GetMapping(value = "/ping")
	public String ping() {
		return "Servicio funcionando.";
	}
	
	@GetMapping(value = "/all")
	public List<User> findAll() {
		return userService.findAll();
	}
	
	@GetMapping(value = "/get-user/{userId}")
	public User getUser(@PathVariable("userId") Long id) {
		return userService.getUser(id);
	}
	
	@PostMapping(value = "/edit/{userId}")
	public User edit(@PathVariable("userId") Long id, @RequestBody User userValues) {
		return userService.updateUser(id, userValues); 
	}

	@PostMapping(value = "/save")
	public User save(@RequestBody User user) {		
		return userService.saveUser(user);
	}
	
	@DeleteMapping(value = "/delete/{userId}")
	public boolean delete(@PathVariable("userId") Long id) {		
		return userService.deleteUser(id);
	}
}
