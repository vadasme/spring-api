package com.blulatam.fase2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiBlulatamFase2Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiBlulatamFase2Application.class, args);
	}

}
