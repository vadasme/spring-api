package com.blulatam.fase2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.blulatam.fase2.entity.User;
import com.blulatam.fase2.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	@Autowired
    private UserRepository userRepository;

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}
	
	@Override
	public User getUser(long id) {
		// TODO Auto-generated method stub
		try {
			return userRepository.findById(id).get();
		} catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ex.getMessage(), ex);
		}
	}
	
	@Override
	public User updateUser(long id, User userValues) {
		try {
			User userFound = getUser(id);
			
			userFound.setName(userValues.getName());
			userFound.setSurname(userValues.getSurname());
			userFound.setEmail(userValues.getEmail());
			userFound.setPhone(userValues.getPhone());
			
			return userRepository.save(userFound);
		} catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ex.getMessage(), ex);
		}
	}
	
	@Override
	public User saveUser(User userValues) {
		try {
			User user = new User();
			user.setName(userValues.getName());
			user.setSurname(userValues.getSurname());
			user.setEmail(userValues.getEmail());
			user.setPhone(userValues.getPhone());
			
			return userRepository.save(user);
		} catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ex.getMessage(), ex);
		}
	}
	
	@Override
	public Boolean deleteUser(long id) {
		try {
			User userFound = getUser(id);
			userRepository.delete(userFound);
			return true;
		} catch (Exception ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    ex.getMessage(), ex);
		}
	}

}
