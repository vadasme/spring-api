package com.blulatam.fase2.service;

import com.blulatam.fase2.entity.User;

import java.util.List;

public interface UserService {
	
	public List<User> findAll();

	User getUser(long id);
	
	User updateUser(long id, User userValues);

	User saveUser(User userValues);
	
	Boolean deleteUser(long id);

}
